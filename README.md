# climate-articles-pageviews

This repository contains the code to calculate the number of pageviews received by Wikipedia articles on climate change.

# How this works?

### 1) Seed articles from WikiProject in English Wikipedia
The starting point is the list of articles in the WikiProject Climate change in English Wikipedia: https://en.wikipedia.org/wiki/Wikipedia:WikiProject_Climate_change
The list of articles can be tracked in the following category: https://en.wikipedia.org/wiki/Category:WikiProject_Climate_change_articles
Note that the articles are all in namespace 1 (talk pages) since this is where the annotation for the WikiProject is captured

A nice way to query this list automatically is the petscan-tool via this query: https://petscan.wmflabs.org/?psid=23389292
* Filter redirects, soft redirects, disambiguation (page properties)
* Convert to main namespace such that page-id corresponds to the actual article and not the talk-page (other sources); the page-id is preferred when looking for pageviews.
* Add qid (wikidata)
* Under Output: Format "CSV"

### 2) Get corresponding articles across all wikis
For each article from English Wikipedia, we get the corresponding article in all other wikipedias by matching the wikidata-item using the wikidata_item_page_links table: https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Content/Wikidata_item_page_link
We keep the page-id and the page-title of each matched item in each Wikipedia language version.

We only keep unique page-titles. After matching via wikidata-items, there are some duplicate page-titles. This seems to come from the wikidata-item-page-links table which lists the same page-title with different page-ids

Run: wiki-projects_climate_get-articles.ipynb

### We add the pageviews
For each page-title we get the number of pageviews in the corresponding Wikipedia language version in a given time-period using the pageviews API.
We add the pageviews from all redirects to that page.


Run: wiki-projects_climate_add-pageviews.ipynb


Notes
* the notebooks are run on the WMF analytics-cluster (stat-machines): https://wikitech.wikimedia.org/wiki/Analytics/Systems/Clients
* it uses a standard conda-analytics environment: https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Conda